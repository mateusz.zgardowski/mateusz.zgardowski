# Hi, I'm Matthew! 👋
I am Frontend Developer

## 🛠 Skills
Next.js • React • React Native • Redux • RTKQuery • Zustand • Axios • Jest • React Testing Library • JavaScript • TypeScript • ES6 • GraphQL • ContentfulCMS • HTML5 • CSS • SCSS • Styled Components • TailwindCSS • Git • GitLab/GitHub • Storybook

## 🚀 About Me
I have 3 years of commercial experience developing e-commerce and mobile applications in React Native. I am constantly growing, exploring React, Next.js and React Native, and my goal is to become a Fullstack Developer. I am currently learning NestJS backend technology, expanding my skills to create complete web applications.

## 🔗 Links
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/mateuszzgardowski/)

## 👀 What I'm working on
🦾 I am currently working on MyDietApp as a portfolio application and I am a contributor to Up2Date

🧠 I'm currently working React, Next.js and React Native. After hours I spend my time to learn NestJS for backend. 
